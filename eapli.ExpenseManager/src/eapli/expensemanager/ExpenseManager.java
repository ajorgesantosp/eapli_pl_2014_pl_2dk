/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager;

import eapli.expensemanager.persistence.PaymentMeanRepository;
import eapli.expensemanager.presentation.MenuUI;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseManager {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
//                bootStrap();
		MenuUI menu = new MenuUI();
		menu.run();
	}
        /**
         * this method starts the inicial conditions of the app
         */
    protected static void bootStrap(){
        final eapli.expensemanager.domain.Cash cash;
             cash = new eapli.expensemanager.domain.Cash();
        eapli.expensemanager.persistence.PersistenceFactory p = 
                eapli.expensemanager.persistence.PersistenceFactory.getInstance();
        
        eapli.expensemanager.persistence.RepositoryFactory fact = 
                p.getRepositoryFactory();
        
        eapli.expensemanager.persistence.PaymentMeanRepository myrep = fact.getPaymentMeanRepository();
        myrep.save(cash);
    }
}