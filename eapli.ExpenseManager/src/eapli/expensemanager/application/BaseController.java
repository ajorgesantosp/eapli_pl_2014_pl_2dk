
package eapli.expensemanager.application;

import eapli.expensemanager.domain.MonthPage;
import eapli.expensemanager.persistence.MonthPageRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.RepositoryFactory;
import eapli.expensemanager.persistence.jpa.MonthPageRepositoryimpl;
import eapli.util.DateTime;
import java.util.Calendar;

/**
 *
 * @author Ivo Padrão e Andre Casaca
 */
public abstract class BaseController {
    
     /**
     * Este metodo devolve o valor das despesas do mes corrente
     * @return monthPage.getExpensesCurrentTotal()
     */

    public double getThisMonthExpenditure() {
        MonthPage monthPage = getCurrentMonthPage();
        return monthPage.getExpensesCurrentTotal();
    }
    
    /**
     * Neste metodo e devolvido o objecto MonthPage correspondente ao mes corrente
     * @return monthPage
     */
    
    private MonthPage getCurrentMonthPage() {

        int year = DateTime.currentYear();
        int month = DateTime.currentMonth();
        PersistenceFactory persist = PersistenceFactory.getInstance();
        RepositoryFactory fact = persist.getRepositoryFactory();
        MonthPageRepository rep = fact.getMonthPageRepository();
        MonthPage monthPage = rep.retrieveMonthPage(year, month);
        return monthPage;
    }
    
    /**
     * This method calculates the dates between the current week and returns the total expenses of that week
     * @return totalWeekExpenses
     */
      public double returnCurrentWeekExpenses() {
          
        PersistenceFactory persist = PersistenceFactory.getInstance();
        RepositoryFactory repo = persist.getRepositoryFactory();
        MonthPageRepository monthPageRepo = repo.getMonthPageRepository();
        double totalWeekExpense = 0;
        //Ano da semana corrente
        int currentYear = DateTime.currentYear();
        //Número da semana corrente
        int weekNumber = DateTime.currentWeekNumber();
        //Data do ínicio da semana (sempre uma segunda-feira)
        Calendar beginningOfWeek = DateTime.beginningOfWeek(currentYear, weekNumber);
        //Data de hoje
        Calendar actualDate = DateTime.today();

        MonthPage month1;
        MonthPage month2;
        Calendar beginningOfMonth;
        Calendar endOfMonth;

        //A semana só pertence a um mês
        if (beginningOfWeek.get(Calendar.MONTH)
                == actualDate.get(Calendar.MONTH)) {
            month1 = monthPageRepo.retrieveMonthPage(beginningOfWeek.get(Calendar.MONTH) + 1, beginningOfWeek.get(Calendar.YEAR));

            //Correcção de um erro no número do mês
            beginningOfWeek.add(Calendar.MONTH, 1);
            actualDate.add(Calendar.MONTH, 1);

            totalWeekExpense = month1.getExpensesBetweenDates(beginningOfWeek, actualDate);
        } //A semana fica no meio de dois meses
        else {
            month1 = monthPageRepo.retrieveMonthPage(beginningOfWeek.get(Calendar.MONTH) + 1, beginningOfWeek.get(Calendar.YEAR));
            month2 = monthPageRepo.retrieveMonthPage(beginningOfWeek.get(Calendar.MONTH) + 2, beginningOfWeek.get(Calendar.YEAR));

            //Correcção de um erro no número do mês
            beginningOfWeek.add(Calendar.MONTH, 1);
            actualDate.add(Calendar.MONTH, 1);

            endOfMonth = DateTime.endOfMonth(beginningOfWeek.get(Calendar.YEAR), beginningOfWeek.get(Calendar.MONTH));
            beginningOfMonth = endOfMonth;
            beginningOfMonth.add(Calendar.DATE, 1);

            totalWeekExpense = month1.getExpensesBetweenDates(beginningOfWeek, endOfMonth);
            totalWeekExpense += month2.getExpensesBetweenDates(beginningOfMonth, actualDate);
        }
        return totalWeekExpense;
    }
}
