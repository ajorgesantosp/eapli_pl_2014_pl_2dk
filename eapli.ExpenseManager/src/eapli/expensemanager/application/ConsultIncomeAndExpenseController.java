/*
 * Consulta de Receitas e Despesas
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.IncomeAndExpense;
import eapli.expensemanager.persistence.RepositoryFactory;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.domain.MonthPage;
import eapli.expensemanager.persistence.MonthPageRepository;
import java.util.List;
import javax.persistence.Persistence;

/**
 *
 * @author NT/TC
 */
public class ConsultIncomeAndExpenseController extends BaseController {

   /**
    *
    * @param mon Gets Income And Expense of Month mon
    * @param year Gets Income And Expense of Year year
    * @return Current month income and expense
    */
   public final IncomeAndExpense getIncomeAndExpense(int mon, int year) {
	  PersistenceFactory persist = PersistenceFactory.getInstance();
	  RepositoryFactory factory = persist.getRepositoryFactory();
	  MonthPageRepository mRep = factory.getMonthPageRepository();

	  final MonthPage month = mRep.retrieveMonthPage(year, mon);
	  if (month == null) {
		 return null;
	  }

	  final IncomeAndExpense total = new IncomeAndExpense(
			  month.getIncomeCurrentTotal(), month.getExpensesCurrentTotal());
	  return total;
   }
}
