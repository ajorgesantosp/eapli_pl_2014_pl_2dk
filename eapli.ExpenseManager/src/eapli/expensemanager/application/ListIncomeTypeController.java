/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.application;

import eapli.expensemanager.domain.IncomeType;
import eapli.expensemanager.persistence.IncomeTypeRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.RepositoryFactory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author i070375
 */
public class ListIncomeTypeController extends BaseController{
    public List<IncomeType> listIncomeType() {
   
        PersistenceFactory persist = PersistenceFactory.getInstance();
        RepositoryFactory repoFactory = persist.getRepositoryFactory();
        IncomeTypeRepository repo = repoFactory.getIncomeTypeRepository();

        List<IncomeType> data = new ArrayList<IncomeType>(); 
        data = repo.listIncomeType();
        return data;
    }
}
