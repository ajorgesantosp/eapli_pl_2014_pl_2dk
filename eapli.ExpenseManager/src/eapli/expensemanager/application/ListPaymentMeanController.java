/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.PaymentMean;
import eapli.expensemanager.persistence.PaymentMeanRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.RepositoryFactory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fabio
 */
public class ListPaymentMeanController extends BaseController {

    public List<PaymentMean> getAllPaymentMeans() {

        PersistenceFactory persist = PersistenceFactory.getInstance();
        RepositoryFactory factory = persist.getRepositoryFactory();
        PaymentMeanRepository repo = factory.getPaymentMeanRepository();

        List<PaymentMean> data = new ArrayList<PaymentMean>();
        data = repo.getAllPaymentMeans();
        return data;
    }
}
