/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.Expense;
import eapli.expensemanager.domain.ExpenseType;
import eapli.expensemanager.domain.MonthPage;
import eapli.expensemanager.persistence.ExpenseTypeRepository;
import eapli.expensemanager.persistence.MonthPageRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.RepositoryFactory;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author i110546
 */
public class RegisterExpenseController extends BaseController {

    /**
     * This is the controller used to register a simple expense.
     *
     * @param description This is the description of the expense.
     * @param value This is the value of the expense.
     * @param repository This is the monthly page repository that contains all
     * the pages.
     */
    PersistenceFactory persist = PersistenceFactory.getInstance();
    RepositoryFactory factoryRepository = persist.getRepositoryFactory();
    MonthPageRepository monthPageRepository = factoryRepository.getMonthPageRepository();
    
    public void registerExpense(double value, String description, Calendar date, ExpenseType exType) {
        Expense expense = new Expense(value, description, date, exType);
        MonthPage page = monthPageRepository.retrieveMonthPage(expense.getDate().YEAR, expense.getDate().MONTH);
        page.addExpense(expense);
        monthPageRepository.updateMonthPage(page);
    }

    public List<ExpenseType> getExpenseTypeList() {
        PersistenceFactory persist = PersistenceFactory.getInstance();
        RepositoryFactory RepositoryFactory = persist.getRepositoryFactory();
        ExpenseTypeRepository exType = RepositoryFactory.getExpenseTypeRepository();
        List<ExpenseType> exTypeList = exType.retrieveExpenseTypeList();
        return exTypeList;
    }
}
