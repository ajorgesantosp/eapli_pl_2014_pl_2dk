/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.application;

import eapli.expensemanager.domain.ExpenseType;
import eapli.expensemanager.persistence.ExpenseTypeRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.RepositoryFactory;
import eapli.expensemanager.persistence.jpa.ExpenseTypeRepositoryimpl;
import eapli.expensemanager.persistence.MonthPageRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.RepositoryFactory;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class RegisterExpenseTypeController extends BaseController {

    PersistenceFactory persist = PersistenceFactory.getInstance();
    RepositoryFactory factory = persist.getRepositoryFactory();
    MonthPageRepository mRep = factory.getMonthPageRepository();

    public void registerExpenseType(String expenseTypeText) {
        ExpenseTypeRepository repo = factory.getExpenseTypeRepository();
        ExpenseType expenseType = new ExpenseType(expenseTypeText);
        PersistenceFactory persist = PersistenceFactory.getInstance();
        RepositoryFactory factory = persist.getRepositoryFactory();
        repo.save(expenseType);
    }
}
