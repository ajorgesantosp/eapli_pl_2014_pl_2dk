package eapli.expensemanager.application;

import eapli.expensemanager.domain.Income;
import eapli.expensemanager.domain.MonthPage;
import eapli.expensemanager.persistence.MonthPageRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.jpa.MonthPageRepositoryimpl;
import eapli.expensemanager.persistence.RepositoryFactory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Filipe
 */
public class RegisterIncomeController extends BaseController{
//    private static List<Income> incomeList = new ArrayList();
    public void registerIncome(int amount, Calendar date, String description/*, IncomeType type*/) {
        /* TODO use a factory to obtain a repository object*/
        PersistenceFactory persist = PersistenceFactory.getInstance();
        RepositoryFactory factory = persist.getRepositoryFactory();
        MonthPageRepository monthPageRepository = factory.getMonthPageRepository();
        MonthPage monthPage = monthPageRepository.retrieveMonthPage(date.get(Calendar.MONTH), date.get(Calendar.YEAR));
        Income income = new Income(amount, date, description/*, type*/);
//        incomeList.add(income);
//        for(int i=0; i<incomeList.size(); i++){
//            monthPage.addIncome(incomeList.get(i));
//        }
        monthPage.addIncome(income);
        int month = date.get(Calendar.MONTH);
        month++;
        int year = date.get(Calendar.YEAR);
        monthPageRepository.save(monthPage);
        System.out.println("A receita atual do Mês: "+month+" ; Ano: "+year
                +" é "+monthPage.getIncomeCurrentTotal());
    }
}
