/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.application;

import eapli.expensemanager.domain.IncomeType;
import eapli.expensemanager.persistence.IncomeTypeRepository;
import eapli.expensemanager.persistence.PersistenceFactory;
import eapli.expensemanager.persistence.RepositoryFactory;

/**
 *
 * @author i070375
 */
public class RegisterIncomeTypeController extends BaseController{
    
    public void registerIncomeType(String incomeTypeText) {
        
            IncomeType incomeType = new IncomeType(incomeTypeText);

            PersistenceFactory persist = PersistenceFactory.getInstance();
            RepositoryFactory repoFactory = persist.getRepositoryFactory();

            IncomeTypeRepository repo = repoFactory.getIncomeTypeRepository();

            repo.save(incomeType);
            
            

	}
}
