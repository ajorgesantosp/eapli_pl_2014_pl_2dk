package eapli.expensemanager.application;

import eapli.expensemanager.domain.Check;
import eapli.expensemanager.domain.CreditCard;
import eapli.expensemanager.domain.DebitCard;
import eapli.expensemanager.persistence.RepositoryFactory;
import java.util.Calendar;
import eapli.expensemanager.domain.Cash;
import eapli.expensemanager.persistence.PaymentMeanRepository;
import eapli.expensemanager.persistence.PersistenceFactory;

/**
 *
 * @author Fabio 1080457
 */
public class RegisterPaymentMeanController extends BaseController {

    PersistenceFactory persist = PersistenceFactory.getInstance();
    RepositoryFactory factory = persist.getRepositoryFactory();
    PaymentMeanRepository repo = factory.getPaymentMeanRepository();

    public void registerCreditCard(String cardName, String bankName, String cardNumber, String name, Calendar valCard) {
        CreditCard creditCard = new CreditCard(cardName, bankName, cardNumber, name, valCard);
        repo.save(creditCard);
    }

    public void registerDebitCard(String cardName, String bankName, String cardNumber, String name, Calendar valCard) {
        DebitCard debitCard = new DebitCard(cardName, bankName, cardNumber, name, valCard);
        repo.save(debitCard);
    }

    public void registerCheck(String bank, String accountNumber, Calendar validity) {
        Check check = new Check(bank, accountNumber, validity);
        repo.save(check);
    }

    public void registerCash() {
        Cash cash = new Cash();
        repo.save(cash);
    }
}
