package eapli.expensemanager.domain;

import eapli.util.Validations;
import java.util.Calendar;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.Table;

/**
 * This class instantiate a CreditCard or DebitCard
 *
 * @author Fabio 1080457
 */
@Entity
@Table(name = "CARD")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Card extends PaymentMean implements Serializable {

    String cardNumber;
    String cardName;
    String bankName;
    String name;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar valCard;

    public Card() {
    }

    /**
     *
     * @param cardName
     * @param bankName
     * @param cardNumber
     * @param name
     * @param valCard
     */
    public Card(String cardName, String bankName, String cardNumber, String name, Calendar valCard) {
        if (Validations.isNullOrEmpty(cardName) || Validations.isNullOrEmpty(bankName)
                || Validations.isNullOrEmpty(cardNumber) || Validations.isNullOrEmpty(name) || valCard == null) {
            throw new IllegalArgumentException();
        }
        this.cardName = cardName;
        this.bankName = bankName;
        this.cardNumber = cardNumber;
        this.name = name;
        this.valCard = valCard;
    }
}
