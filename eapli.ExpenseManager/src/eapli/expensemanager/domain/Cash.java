/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author LCLS
 */

@Entity
@Table(name
        = "Cash")

public class Cash extends PaymentMean implements Serializable {

    public Cash() {

    }

    public Cash(float amount) {

    }

    @Override
    public String getDescription() {
        return "Cash";
    }

}
