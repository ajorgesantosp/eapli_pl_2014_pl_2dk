package eapli.expensemanager.domain;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.Table;

@Entity
@Table(name
        = "BankCheck")

public class Check extends PaymentMean implements Serializable {

    String bank;
    String accountNumber;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar checkValidity;

    
    
    
    public Check() {
    }

    public Check(String bank, String accountNumber, Calendar checkValidity) {
        this.bank = bank;
        this.accountNumber = accountNumber;
        this.checkValidity = checkValidity;
    }

    @Override
    public String getDescription() {
        SimpleDateFormat simpleFormat = new SimpleDateFormat("dd-MM-yyyy");
        String checkVal = simpleFormat.format(checkValidity.getTime());
        return "\n* Check *\n\tBank: " + bank + "\n\tAccountNumber: " + accountNumber + "\n\tCheck Validity: " + checkVal;
    }
}
