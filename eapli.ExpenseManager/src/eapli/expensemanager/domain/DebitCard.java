package eapli.expensemanager.domain;

import java.util.Calendar;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * This class represents a debit card and is a child of card class.
 *
 * @author Fabio 1080457
 */
@Entity
@Table(name = "DEBITCARD")
public class DebitCard extends Card implements Serializable {

    /**
     * Create new Debit Card.
     *
     * @param cardName type card
     * @param bankName bank name
     * @param cardNumber Number on card
     * @param name name on card
     * @param valCard expiration date card
     */
    public DebitCard() {
    }

    public DebitCard(String cardName, String bankName, String cardNumber, String name, Calendar valCard) {
        super(cardName, bankName, cardNumber, name, valCard);
    }

    @Override
    public String getDescription() {
        SimpleDateFormat simpleFormat = new SimpleDateFormat("dd-MM-yyyy");
        String validity = simpleFormat.format(valCard.getTime());
        return "\n* Debit Card *\n\tCard Name: " + cardName
                + "\n\tBank: " + bankName
                + "\n\tAccount Number: " + cardNumber
                + "\n\tName: " + name
                + "\n\tValidity: " + validity;
    }

}
