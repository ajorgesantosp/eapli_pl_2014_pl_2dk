/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author i110546
 */
@Entity
@Table(name = "Expense")
@Inheritance(strategy = InheritanceType.JOINED)
public class Expense implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idExpense;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar date;
    
    private double value;
    private String description;
    @ManyToOne (cascade = CascadeType.ALL)
    private ExpenseType exType;

    public Expense() {
    }

    public Expense(double value, String description, Calendar date, ExpenseType exType) {
        setValue(value);
        setDescription(description);
        this.date = date;
        this.exType = exType;
    }

    //Private set methods for validation purposes ONLY
    private void setValue(double value) {
        if (value <= 0) {
            throw new IllegalArgumentException();
        } else {
            this.value = value;
        }
    }

    private void setDescription(String description) {
        if (description.isEmpty()) {
            throw new IllegalArgumentException();
        } else {
            this.description = description;
        }
    }

    public Calendar getDate() {
        return date;
    }

    public double getValue() {
        return value;
    }
}
