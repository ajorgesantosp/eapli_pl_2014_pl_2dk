/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "ExpenseType")
@Inheritance(strategy = InheritanceType.JOINED)
public class ExpenseType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idExpenseType;

    private String text;

    public ExpenseType() {
    }

    public ExpenseType(String text) {

//		if (Validations.isNullOrEmpty(text) ) {
//			throw new IllegalArgumentException();
//		}
        this.text = text;
    }
    
    @Override
    public String toString() {
        return text;
    }
}
