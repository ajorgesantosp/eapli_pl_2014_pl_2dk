package eapli.expensemanager.domain;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Filipe
 */

@Entity
public class Income implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idIncome;
    
    private int amount;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar date;
    
    private String description;
    /*private IncomeType type;*/

    public Income() {
    }
    
    public Income(int amount, Calendar date, String description/*, IncomeType type*/) {
        // TODO use Validations utilty class
	if (description == null || amount <= 0) {
            throw new IllegalArgumentException();
	}
	this.amount = amount;
        this.date = date;
        this.description = description;
        /*this.type = type;*/
    }
    
    public int getAmount(){
        return amount;
    }
    
    public String toString(){
        int month = date.get(Calendar.MONTH);
        month++;
        return "Amount = "+amount+" ; Day = "+date.get(Calendar.DAY_OF_MONTH)
                +" ; Month = "+month+" ; Year = "+date.get(Calendar.YEAR)
                +" ; Description = "+description;
    }
}
