/*
 * Consulta de Receitas e Despesas
 */
package eapli.expensemanager.domain;

/**
 *
 * @authors NT/TC
 */
public class IncomeAndExpense {

   private final int incomeTotal;
   private final double expenseTotal;

   /**
    *
    * @param income Gets Total income
    * @param expense Gets Total expense
    */
   public IncomeAndExpense(int income, double expense) {
	  incomeTotal = income;
	  expenseTotal = expense;
   }

   /**
    *
    * @return incomeTotal
    */
   public int getIncomeTotal() {
	  return incomeTotal;
   }

   /**
    *
    * @return expenseTotal
    */
   public double getExpenseTotal() {
	  return expenseTotal;
   }
}
