/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author i070375
 */
@Entity
public class IncomeType implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idIncomeType;
        private String type;

    public IncomeType() {
    }
    
	public IncomeType(String type) {
		// TODO use Validations utilty class
		if ((type == null) || (type.isEmpty())) {
			throw new IllegalArgumentException();
		}
                else{
                    this.type = type;
                }
	}

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
