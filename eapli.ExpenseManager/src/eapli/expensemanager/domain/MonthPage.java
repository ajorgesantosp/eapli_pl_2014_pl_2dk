/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Marcos | @Altered by Tiago Cardoso
 */

@Entity
public class MonthPage implements Serializable {
    
    @Id
    @GeneratedValue/*(strategy = GenerationType.IDENTITY)*/
    private int idMonthPage;
    
    @OneToMany(cascade= CascadeType.ALL)
    private List<Expense> expenses = new ArrayList(); //necessário inicializar a variável para funcionar -> Marco
    
    @OneToMany(cascade= CascadeType.ALL)
    private List<Income> incomes = new ArrayList();
    
    private int year;
    private int month;
    
    public MonthPage(){
        
    }
    
    public MonthPage(int year, int month) {
        this.year = year;
        this.month = month;
    }
    
    public int getYear() {
        return year;
    }
    
    public int getMonth() {
        return month;
    }
    
    /**Add a new expense to the page**/
    public void addExpense(Expense expense) {
        expenses.add(expense);
    }
    
    //Add a new income to the page
    public void addIncome(Income income){
        incomes.add(income);
    }
    
    public List<Income> getIncomeList(){
        return incomes;
    }
    
    public List<Expense> getExpenseList(){
        return expenses;
    }
    
    public double getExpensesCurrentTotal(){
        double total =0;
        for (int i=0; i<expenses.size();i++){
            Expense e = expenses.get(i);
            total+= e.getValue();
        }//to do -> fix with iterator
        return total;
    }
    
    public int getIncomeCurrentTotal(){
        int total = 0;
        if(!incomes.isEmpty()){
            for (int i=0; i<incomes.size();i++){
                Income in = incomes.get(i);
                total+= in.getAmount();
                System.out.println(in.toString());
            }//to do -> fix with iterator
        }
        return total;
    }
    
    public double getExpensesBetweenDates(Calendar date1, Calendar date2){
        double total = 0;
        
        List<Expense> exp = getExpenseList();
        
        if(exp.isEmpty()){
            return 0;
        }
        
        else{
            for(int i = 0; i < exp.size(); i++){
                //Verifica se a data das despesas no arraylist "exp" desta monthPage se encontra entre a data1 e data2
                if(exp.get(i).getDate().compareTo(date1) >= 0 && exp.get(i).getDate().compareTo(date2) <= 0){
                    total += exp.get(i).getValue();
                }
            }
            return total;
        }
    }
}
