package eapli.expensemanager.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * Represents a payment mean.
 *
 * @author Fabio 1080457
 */
@Entity
@Table(name = "PAYMENTMEAN")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PaymentMean implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idPaymentType;

    public PaymentMean() {
    }

    public abstract String getDescription();

}
