/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence;

import eapli.expensemanager.domain.ExpenseType;
import java.util.List;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public interface ExpenseTypeRepository {

	public void save(ExpenseType expenseType);

	//public int count();
        
        public List<ExpenseType> retrieveExpenseTypeList();
}
