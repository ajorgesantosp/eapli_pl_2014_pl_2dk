/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence;
import eapli.expensemanager.domain.IncomeType;
import java.util.List;

/**
 *
 * @author i070375
 */
public interface IncomeTypeRepository {
    
	public void save(IncomeType incomeType);

	public int count();
        
        public List<IncomeType> listIncomeType();
	
}
