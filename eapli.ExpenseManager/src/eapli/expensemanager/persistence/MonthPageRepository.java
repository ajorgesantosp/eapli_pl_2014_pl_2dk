/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence;

import eapli.expensemanager.domain.MonthPage;

/**
 *
 * @author 1100496
 */
public interface MonthPageRepository {
    
    public MonthPage retrieveMonthPage(int year, int month);
    
    public void createMonthPage(MonthPage page);
    
    public void updateMonthPage(MonthPage monthPage);
    
    public void save(MonthPage monthPage);

    //public int count();
}
