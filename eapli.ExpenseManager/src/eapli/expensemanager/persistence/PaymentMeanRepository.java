package eapli.expensemanager.persistence;

import eapli.expensemanager.domain.PaymentMean;
import java.util.List;

public interface PaymentMeanRepository {

    public void save(PaymentMean paymentMean);

    public List<PaymentMean> getAllPaymentMeans();

    //public int count();
}
