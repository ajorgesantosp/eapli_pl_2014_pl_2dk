package eapli.expensemanager.persistence;

import eapli.expensemanager.persistence.RepositoryFactory;

/**
 *
 * @author NT
 */
public  class PersistenceFactory {

   private static PersistenceFactory instance = new PersistenceFactory();
   private static String persistenceType = "jpa"; 
   private static RepositoryFactory myfactory;
   
   private PersistenceFactory(){
   
}
   public static PersistenceFactory getInstance(){
	  if(("jpa").equalsIgnoreCase(persistenceType)){
		 myfactory = (RepositoryFactory) new eapli.expensemanager.persistence.jpa.RepositoryFactoryJPA();
	  }
	  else{
		 myfactory = (RepositoryFactory) new eapli.expensemanager.persistence.inMemory.FactoryRepositoryInMemory();
	  }
	  return instance;
   }
   
   public RepositoryFactory getRepositoryFactory(){
	  //RepositoryFactory myfactory=null;
	  return myfactory;
   }
//   private final PersistenceFactory FactoryType;
//
//   public PersistenceFactory() {
//	  String strClassName;
//		 strClassName = getRepositoryFactory("FactoryType");
//	  try {
//		 FactoryType = (PersistenceFactory) Class.forName(strClassName).
//				 newInstance();
//	  } catch (Exception ex) {
//		 FactoryType = null;
//	  }
//   }
}
