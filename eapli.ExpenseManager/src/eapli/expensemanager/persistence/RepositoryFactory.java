/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence;



/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public  interface RepositoryFactory {

 public PaymentMeanRepository getPaymentMeanRepository();
 public IncomeTypeRepository getIncomeTypeRepository();
 public  MonthPageRepository getMonthPageRepository();
 public  ExpenseTypeRepository getExpenseTypeRepository();
	
}
