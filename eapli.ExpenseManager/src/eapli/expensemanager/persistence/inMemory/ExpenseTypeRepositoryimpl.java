/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.inMemory;

import eapli.expensemanager.domain.ExpenseType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseTypeRepositoryimpl implements eapli.expensemanager.persistence.ExpenseTypeRepository {

	private static final List<ExpenseType> data = new ArrayList<ExpenseType>();

	public void save(ExpenseType expenseType) {
		if (expenseType == null) {
			throw new IllegalArgumentException();
		}

		data.add(expenseType);
	}

	public int count() {
		return data.size();
	}
        
        @Override
        public List<ExpenseType> retrieveExpenseTypeList() {
            return data;
        }
}
