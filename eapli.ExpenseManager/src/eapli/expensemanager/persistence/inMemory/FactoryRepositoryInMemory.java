/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence.inMemory;

import eapli.expensemanager.persistence.RepositoryFactory;

/**
 *
 * @author NT
 */
public class FactoryRepositoryInMemory implements RepositoryFactory{
   
   public FactoryRepositoryInMemory(){
	  
   }

   public ExpenseTypeRepositoryimpl getExpenseTypeRepository() {
	   ExpenseTypeRepositoryimpl rep = new ExpenseTypeRepositoryimpl();
        return rep;
    }

    public MonthPageRepositoryimpl getMonthPageRepository() {
   
            MonthPageRepositoryimpl rep = new MonthPageRepositoryimpl();
        
        return rep;
    }

    public IncomeTypeRepositoryimpl getIncomeTypeRepository() {
        IncomeTypeRepositoryimpl rep = new IncomeTypeRepositoryimpl();
				return rep;
    }
    
    public PaymentMeanRepositoryimpl getPaymentMeanRepository() {
        return new PaymentMeanRepositoryimpl();
    }
}


