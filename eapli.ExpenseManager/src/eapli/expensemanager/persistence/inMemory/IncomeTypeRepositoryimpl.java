/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence.inMemory;

import eapli.expensemanager.domain.IncomeType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author i070375
 */
public class IncomeTypeRepositoryimpl implements eapli.expensemanager.persistence.IncomeTypeRepository {
    
    private static final List<IncomeType> data = new ArrayList<IncomeType>();

	public void save(IncomeType incomeType) {
		if (incomeType == null) {
			throw new IllegalArgumentException();
		}

		data.add(incomeType);
	}

	public int count() {
		return data.size();
	}
        
        public List<IncomeType> listIncomeType(){
        
            return data;
        
        
        }

}
