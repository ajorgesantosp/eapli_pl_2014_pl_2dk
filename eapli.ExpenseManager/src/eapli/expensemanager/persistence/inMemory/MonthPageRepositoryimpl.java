/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.inMemory;

import eapli.expensemanager.domain.Income;
import eapli.expensemanager.domain.MonthPage;
import java.util.ArrayList;

/**
 *
 * @author Marcos
 */
public class MonthPageRepositoryimpl implements eapli.expensemanager.persistence.MonthPageRepository{

    private static ArrayList<MonthPage> monthPages;
    
    @Override
    public MonthPage retrieveMonthPage(int year, int month) {//modded by tiagocardoso-  adicionei o metodo exists para uso externo
        MonthPage mon = exists(year,month);
        if (mon!=null){
            return mon;
        }
        MonthPage page = new MonthPage(year, month);
        monthPages.add(page);
        return page;
    
    }
    
    public MonthPage exists(int year, int month){
        for (MonthPage page : monthPages) {
            if (page.getYear() == year && page.getMonth() == month) {
                return page;
            }
        }
        return null;
    }
    
    public void createMonthPage(MonthPage page) {
        monthPages.add(page);
    }
    
    @Override
    public void updateMonthPage(MonthPage monthPage) {
        for (MonthPage page : monthPages) {
            if (page.getYear() == monthPage.getYear() && page.getMonth() == monthPage.getMonth()) {
                monthPages.remove(page);
                monthPages.add(monthPage);
            }
        }
    }
    

    @Override
    public void save(MonthPage monthPage) {
        if (monthPage == null) {
			throw new IllegalArgumentException();
		}
        monthPages.add(monthPage);
}

    public int count() {
        return monthPages.size();
    }
}
