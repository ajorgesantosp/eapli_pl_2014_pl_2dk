/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.inMemory;

import eapli.expensemanager.domain.PaymentMean;
import java.util.ArrayList;
import java.util.List;

public class PaymentMeanRepositoryimpl implements eapli.expensemanager.persistence.PaymentMeanRepository {

    private static final List<PaymentMean> listPaymentMean = new ArrayList<PaymentMean>();

    @Override
    public void save(PaymentMean pm) {
        if (pm == null) {
            throw new IllegalArgumentException();
        }
        listPaymentMean.add(pm);
    }

    @Override
    public List<PaymentMean> getAllPaymentMeans() {
        return listPaymentMean;
    }

    public int count() {
        return listPaymentMean.size();
    }
}
