/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.jpa;

import eapli.expensemanager.domain.ExpenseType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author dei
 */
public class ExpenseTypeRepositoryimpl implements eapli.expensemanager.persistence.ExpenseTypeRepository {

   public void save(ExpenseType expenseType) {
	  if (expenseType == null) {
		 throw new IllegalArgumentException("Expense Type could not be saved, value was null!");
	  }
	  EntityManagerFactory factory = Persistence.createEntityManagerFactory("eapli.ExpenseManagerPU");
	  EntityManager manager = factory.createEntityManager();

	  manager.getTransaction().begin();
	  manager.persist(expenseType);
	  manager.getTransaction().commit();
	  manager.close();
   }
   
   @Override
    public List<ExpenseType> retrieveExpenseTypeList() {

        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("eapli.ExpenseManagerPU");
	EntityManager manager = factory.createEntityManager();
	manager.getTransaction().begin();
        Query queryIncomeType = manager.createQuery("SELECT * FROM "
                + "ExpenseType");
        List<ExpenseType> results = queryIncomeType.getResultList();
	manager.getTransaction().commit();
	manager.close();
        
        return results.subList(0, results.size());
    }
}
