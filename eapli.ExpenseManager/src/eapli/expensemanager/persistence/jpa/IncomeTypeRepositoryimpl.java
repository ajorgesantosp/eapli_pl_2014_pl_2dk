/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence.jpa;

import eapli.expensemanager.domain.IncomeType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author i070375
 */
public class IncomeTypeRepositoryimpl implements eapli.expensemanager.persistence.IncomeTypeRepository {

	public void save(IncomeType incomeType) {
		if (incomeType == null) {
			throw new IllegalArgumentException();
		}
                EntityManagerFactory factory = Persistence.
			createEntityManagerFactory("eapli.IncomeManagerPU");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		manager.persist(incomeType);
		manager.getTransaction().commit();
		manager.close();

	}

    @Override
    public int count() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<IncomeType> listIncomeType() {

        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("eapli.IncomeManagerPU");
	EntityManager manager = factory.createEntityManager();
	manager.getTransaction().begin();
        Query queryIncomeType = manager.createQuery("SELECT * FROM "
                + "IncomeType");
        List<IncomeType> results = queryIncomeType.getResultList();
	manager.getTransaction().commit();
	manager.close();
        
        return results.subList(0, results.size());
    }

}
