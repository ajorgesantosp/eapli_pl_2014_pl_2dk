package eapli.expensemanager.persistence.jpa;

import eapli.expensemanager.domain.PaymentMean;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Fabio 1080457
 */
public class PaymentMeanRepositoryimpl implements eapli.expensemanager.persistence.PaymentMeanRepository {

    public void save(PaymentMean paymentMean) {
        if (paymentMean == null) {
            throw new IllegalArgumentException("Payment Mean could not be saved, value was null!");
        }
        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("eapli.ExpenseManagerPU");
        EntityManager manager = factory.createEntityManager();

        manager.getTransaction().begin();
        manager.persist(paymentMean);
        manager.getTransaction().commit();
        manager.close();
    }

    @Override
    public List<PaymentMean> getAllPaymentMeans() {
        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("eapli.ExpenseManagerPU");
        EntityManager manager = factory.createEntityManager();
        manager.getTransaction().begin();
        String sqlString = "SELECT pm FROM PaymentMean pm";
        Query sqlQuery = manager.createQuery(sqlString);
        final List paymentMeans = sqlQuery.getResultList();
        manager.close();

        return new ArrayList(paymentMeans);
    }
}
