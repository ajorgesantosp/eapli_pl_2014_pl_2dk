/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.persistence.jpa;

import eapli.expensemanager.persistence.*;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class RepositoryFactoryJPA implements RepositoryFactory{

        @Override
    public ExpenseTypeRepository getExpenseTypeRepository() {
	   ExpenseTypeRepositoryimpl rep = new ExpenseTypeRepositoryimpl();
        return rep;
    }

    public MonthPageRepository getMonthPageRepository() {
   
            MonthPageRepository rep = new MonthPageRepositoryimpl();
        
        return rep;
    }

        @Override
    public IncomeTypeRepository getIncomeTypeRepository() {
        return new IncomeTypeRepositoryimpl();
    }
    
        @Override
    public PaymentMeanRepository getPaymentMeanRepository() {
        return new PaymentMeanRepositoryimpl();
    }
}
