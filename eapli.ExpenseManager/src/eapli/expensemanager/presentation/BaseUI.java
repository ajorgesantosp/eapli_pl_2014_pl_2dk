package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author Ivo Padrão e Andre Casaca
 */
public abstract class BaseUI {

    /**
     * @ param SEPARATOR imprime caracteres com a intencao de realcar o valor a ser impresso
     */
    public static final String SEPARATOR = "------------------------------------";

    /**
     * @ param BORDER imprime caracteres com a intencao de realcar o valor a ser impresso
     */
    public static final String BORDER = "+-------------------------------------+";

    /**
     * @ param controller e devolvido o controlador devido tendo em conta a classe UI que é chamada
     * @return
     */
    protected abstract BaseController controller();

    /**
     *
     * @return
     */
    protected abstract boolean run();

    /**
     * @param wantsToExit e o teste de paragem do ciclo recebendo um valor do tipo boolean
     */
    public void mainLoop() {
		boolean wantsToExit;
		do {
			wantsToExit = show();
		} while (!wantsToExit);
	}

    /**
     *
     * @return wantsToExit com o valor do tipo boolean
     */
    public boolean show() {
		boolean wantsToExit = run();
		showBoolean();
		return wantsToExit;
	}

    /**
     * @param monthExpenditure fica com o valor dos gastos mensais devolvido pelo metodo chamado pelo controlador
     */
    public void showBoolean() {
        System.out.println("");
        NumberFormat nF = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        double monthExpenditure = controller().getThisMonthExpenditure();
        System.out.println("Monthly Expenditure: " + monthExpenditure);
        double currentWeekExpenses = controller().returnCurrentWeekExpenses();
        System.out.println("Current Week Expenses: " + currentWeekExpenses);
    }

}
