/*
 * Consulta de Receitas e Despesas
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.ConsultIncomeAndExpenseController;
import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.domain.IncomeAndExpense;
import eapli.expensemanager.domain.MonthPage;
import eapli.util.Console;
import java.util.List;

/**
 * @author NT/TC
 */
class ConsultIncomeAndExpenseUI extends BaseUI {

   ConsultIncomeAndExpenseController controller = new ConsultIncomeAndExpenseController();

   @Override
   protected boolean run() {
	  int year = Console.readInteger("Please enter the year to consult: ");
	  int month = Console.readInteger("Please enter the month to consult: ");
	  try {
		 IncomeAndExpense result = controller.getIncomeAndExpense(year, month);
		 print(result);
	  } catch (NullPointerException e) {
		 System.out.println("Month or Year page not found");
	  }
	  return true;
   }

   private static void print(IncomeAndExpense r) {
	  System.out.print("Total Income: " + r.getIncomeTotal());
	  System.out.print("Total Expense: " + r.getExpenseTotal());
   }

   @Override
   protected BaseController controller() {
	  return controller;
   }
}
