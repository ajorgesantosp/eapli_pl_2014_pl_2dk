/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.ListIncomeTypeController;
import eapli.expensemanager.domain.IncomeType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author i070375
 */
public class ListIncomeTypeUI extends BaseUI {
    
    private ListIncomeTypeController controller = new ListIncomeTypeController();
    
         private void print() {
             ListIncomeTypeController incList = new ListIncomeTypeController();
             List<IncomeType> list = new ArrayList<IncomeType>();
             list=incList.listIncomeType();
             if(!list.isEmpty())
             {
                for (int i = 0;i<list.size();i++)
                {
                            System.out.println(list.get(i).getType());
                }
             }
             else
                 System.out.println("No income type available");
             System.out.println("Press Enter to continue");  
            
             try{System.in.read();}  
             catch(Exception e){}  
	}

        public boolean run(){
            print();
            return true;
        }

 @Override
    protected BaseController controller() {
       return controller;
    }





}
