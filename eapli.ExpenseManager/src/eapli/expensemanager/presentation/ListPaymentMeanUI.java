/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.ListPaymentMeanController;
import eapli.expensemanager.domain.PaymentMean;
import java.util.List;

/**
 *
 * @author Fabio - 1080457
 */
public class ListPaymentMeanUI extends BaseUI {

    private final ListPaymentMeanController controller = new ListPaymentMeanController();

    protected void print() {

        List<PaymentMean> paymentMeans = controller.getAllPaymentMeans();
        for (PaymentMean p : paymentMeans) {
            System.out.println(p.getDescription());
        }
    }

    public boolean run() {
        print();
        return true;
    }

    @Override
    protected BaseController controller() {
        return controller;
    }
}
