/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.util.Console;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 * based on Paulo Sousa code
 */
public class MenuUI {

    public void run() {

        int option = -1;
        do {
            System.out.println("=============================");
            System.out.println("          myMoney            ");
            System.out.println("=============================");

            System.out.println("--- MOVEMENTS ---");
            System.out.println("100. Register Expense");
            System.out.println("101. Register Income");
            System.out.println("\n--- CONSULTS ---");
            System.out.println("200. Consult Monthly Expense / Income ");
            System.out.println("201. List Income Types");
            System.out.println("202. List Payment Means");
            System.out.println("\n--- REGISTER ---");
            System.out.println("300. Register Expense Type");
            System.out.println("301. Register Income Type");
            System.out.println("302. Register Payment Mean");
            System.out.println("--- --- --- --- ---");
            System.out.println("0. Exit\n\n");

            option = Console.readInteger("Please choose an option");
            switch (option) {
                case 0:
                    System.out.println("bye ...");
                    return;
                case 100:
                    final RegisterExpenseUI uc03 = new RegisterExpenseUI();
                    uc03.run();
                    break;
                case 200:
                    final ConsultIncomeAndExpenseUI uc07 = new ConsultIncomeAndExpenseUI();
                    uc07.run();
                case 201:
                    final ListIncomeTypeUI uc10a = new ListIncomeTypeUI();
                    uc10a.run();
                    break;
                case 202:
                    final ListPaymentMeanUI uc02a = new ListPaymentMeanUI();
                    uc02a.run();
                    break;
                case 300:
                    final RegisterExpenseTypeUI uc01 = new RegisterExpenseTypeUI();
                    uc01.run();
                    break;
                case 301:
                    final RegisterIncomeTypeUI uc10 = new RegisterIncomeTypeUI();
                    uc10.run();
                    break;
                case 101:
                    final RegisterIncomeUI uc11 = new RegisterIncomeUI();
                    uc11.run();
                    break;
                case 302:
                    final RegisterPaymentMeanUI uc02 = new RegisterPaymentMeanUI();
                    uc02.run();
                    break;
                default:
                    System.out.println("option not recognized.");
                    break;
            }
        } while (option != 0);
    }
}
