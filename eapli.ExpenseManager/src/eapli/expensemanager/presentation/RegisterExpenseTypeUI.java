/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterExpenseTypeController;
import eapli.util.Console;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class RegisterExpenseTypeUI extends BaseUI {

    private String expenseType;
    private RegisterExpenseTypeController controller = new RegisterExpenseTypeController();

    private void submit() {
        controller.registerExpenseType(expenseType);
        System.out.println("\nExpense type recorded!");
    }

    @Override
    protected boolean run() {
        expenseType = Console.readLine("Enter expense type description » ");
        submit();

        return true;
    }

    @Override
    protected BaseController controller() {
        return controller;
    }

}