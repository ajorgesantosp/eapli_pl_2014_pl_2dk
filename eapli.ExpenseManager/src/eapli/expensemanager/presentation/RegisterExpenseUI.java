/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterExpenseController;
import eapli.expensemanager.domain.ExpenseType;
import eapli.util.Console;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author i110546
 */
public class RegisterExpenseUI extends BaseUI {

    private String description;
    private double value;
    private Calendar date;
    private ExpenseType exType;
    RegisterExpenseController controller = new RegisterExpenseController();

    /**
     * This method retrieves the user's input in order to register a simple
     * expense.
     */
    @Override
    public boolean show() {
        description = Console.readLine("Insert the expense's description.");
        value = Console.readDouble("Insert the expense's value.");
        date = Console.readCalendar("Insert the expense's date.");

        //Retrieve all expense types from Controller
        List<ExpenseType> expenseTypes = controller.getExpenseTypeList();

        int counter = 1;
        for (ExpenseType obj : expenseTypes) {
            System.out.println(counter + " - " + obj.toString());
            counter++;
        }
        
        int option = Console.readInteger("Choose an expense type.");
        while(option >= counter || option <= 0) {
            option = Console.readInteger("Invalid option. Choose again");
        }
        
        exType = expenseTypes.get(option - 1);
        
        return true;
    }

    /**
     * In this method we call the RegisterExpenseController with the inserted
     * data.
     */
    private void submit() {
        controller.registerExpense(value, description, date, exType);
    }

    /**
     * This method is called in the beginning to run the User Interface
     * responsible for registering a simple expense.
     */
    @Override
    public boolean run() {
        show();
        submit();
        return true;
    }

    @Override
    protected BaseController controller() {
        return controller;
    }
}
