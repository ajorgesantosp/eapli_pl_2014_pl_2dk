/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.presentation;
import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterIncomeTypeController;
import eapli.util.Console;

/**
 *
 * @author i070375
 */
public class RegisterIncomeTypeUI extends BaseUI{
    
    private String incomeType;
    private RegisterIncomeTypeController controller = new RegisterIncomeTypeController();



	private void submit() {
		RegisterIncomeTypeController controller = new RegisterIncomeTypeController();
		controller.registerIncomeType(incomeType);
                System.out.println("\nIncome type recorded!");
	}
        
        @Override
        public boolean run(){
            incomeType  = Console.readLine("Enter income type » ");
            submit();
            return true;
        }
        
        @Override
	protected BaseController controller() {
		return controller;
	}

           
}
