package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterIncomeController;
import eapli.expensemanager.persistence.inMemory.MonthPageRepositoryimpl;
/*import eapli.expensemanager.persistence.IncomeTypeRepository;*/
import eapli.util.Console;
import eapli.util.DateTime;
import java.util.Calendar;

/**
 *
 * @author Filipe
 */
public class RegisterIncomeUI extends BaseUI{
    private int amount;
    private Calendar date;
    private String description;
    /*private IncomeType type;*/
    private RegisterIncomeController controller = new RegisterIncomeController();

	private void submit() {
            RegisterIncomeController controller = new RegisterIncomeController();
            controller.registerIncome(amount, date, description/*, type*/);
	}

        @Override
        protected BaseController controller() {
            return controller;
        }

        @Override
        protected boolean run() {
            do{
                    amount = Integer.parseInt(Console.readLine("Enter the amount of income (larger than 0) >>"));
            }while(amount <= 0);
            int day = Integer.parseInt(Console.readLine("Enter the day of income >>"));
            int month = Integer.parseInt(Console.readLine("Enter the month of income >>"));
            int year = Integer.parseInt(Console.readLine("Enter the year of income >>"));
            date = DateTime.newCalendar(year, month, day);
    //        System.out.println(date.getTime()); Code used to validate the attribute date
            description = Console.readLine("Enter the description of income >>");
            /*String typeDesc;
            do{
                typeDesc = Console.readLine("Enter the type of income >>");
            }while(!IncomeType.validateIncomeType(typeDesc));
            type = new IncomeType(typeDesc);*/
            
            submit();
            return true;
        }
}
