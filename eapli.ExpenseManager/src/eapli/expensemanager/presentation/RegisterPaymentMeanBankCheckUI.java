package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterPaymentMeanController;
import eapli.util.Console;
import java.util.Calendar;

public class RegisterPaymentMeanBankCheckUI extends BaseUI {

    private String bank;
    private String accountNumber;
    private String description;
    private Calendar valCheck;

    private RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

    public void showRegisterPaymentMeanBankCheck() {
        bank = Console.readLine("Check bank: ");
        accountNumber = Console.readLine("Check account number: ");
        valCheck = Console.readCalendar("Check validaty (dd-mm-aaaa): ");
    }

    public void submitRegisterPaymentMeanBankCheck() {
        RegisterPaymentMeanController controller = new RegisterPaymentMeanController();
        controller.registerCheck(bank, accountNumber, valCheck);
        System.out.println("\nCheck recorded!");
    }

    @Override
    public boolean run() {
        showRegisterPaymentMeanBankCheck();
        submitRegisterPaymentMeanBankCheck();
        return true;
    }

    @Override
    protected BaseController controller() {
        return controller;
    }
}
