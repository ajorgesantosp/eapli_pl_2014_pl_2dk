package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterPaymentMeanController;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author Fabio 1080457
 */
public class RegisterPaymentMeanCreditUI extends BaseUI {

    private String cardName;
    private String bankName;
    private String cardNumber;
    private String name;
    private Calendar valCard;
    private String description;
    private RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

    private void readCreditCard() {
        cardName = Console.readLine("Card Name: ");
        bankName = Console.readLine("Bank: ");
        cardNumber = Console.readLine("Card number: ");
        name = Console.readLine("Name on card: ");
        valCard = Console.readCalendar("Validate (dd-MM-yyyy): ");
    }

    public void submitCreditCard() {
        controller.registerCreditCard(cardName, bankName, cardNumber, name, valCard);
        System.out.println("\nCredit Card " + cardName + " recorded!\n\n");
    }

    @Override
    public boolean run() {
        readCreditCard();
        submitCreditCard();
        return true;
    }

    @Override
    protected BaseController controller() {
        return controller;
    }
}
