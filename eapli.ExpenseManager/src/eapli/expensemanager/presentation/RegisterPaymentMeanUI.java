package eapli.expensemanager.presentation;

import eapli.expensemanager.application.BaseController;
import eapli.expensemanager.application.RegisterPaymentMeanController;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author Fabio
 */
public class RegisterPaymentMeanUI extends BaseUI{

    String cardName;
    String bankName;
    String cardNumber;
    String name;
    Calendar valCard;
    
    private RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

    public boolean run() {
        int option = -1;
        do {
            System.out.println("=============================");
            System.out.println("   REGISTER A PAYMENT MEAN   ");
            System.out.println("=============================\n");
            System.out.println("1. Credit card");
            System.out.println("2. Debit card");
            System.out.println("3. Check");
            System.out.println("0. Return back\n\n");

            option = Console.readInteger("Please choose an option");
            switch (option) {
                case 0:
                    MenuUI menu = new MenuUI();
                    menu.run();
                case 1:
                    RegisterPaymentMeanCreditUI menuCredit = new RegisterPaymentMeanCreditUI();
                    menuCredit.run();
                    break;
                case 2:
                    RegisterPaymentMeanDebitUI menuDebit = new RegisterPaymentMeanDebitUI();
                    menuDebit.run();
                    break;
                case 3:
                    RegisterPaymentMeanBankCheckUI menuCheck = new RegisterPaymentMeanBankCheckUI();
                    menuCheck.run();
                    break;
                default:
                    System.out.println("option not recognized.");
                    break;
            }
        } while (option != 0);
        return true;
    }

    @Override
    protected BaseController controller() {
        return controller;
    }
}
