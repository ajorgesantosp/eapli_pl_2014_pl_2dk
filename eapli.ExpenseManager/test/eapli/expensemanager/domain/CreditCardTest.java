/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.domain;

import eapli.util.DateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fabio
 */
public class CreditCardTest {
    
    public CreditCardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDescription method, of class CreditCard.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        CreditCard instance = new CreditCard("VISA","CGD","18384934","Rui Santos",DateTime.newCalendar(2015, 12, 12));
        String expResult = instance.bankName;
        String result = "CGD";
        assertEquals(expResult, result);
    }
    
}
