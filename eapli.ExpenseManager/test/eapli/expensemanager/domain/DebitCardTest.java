/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.expensemanager.domain;

import eapli.util.DateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fabio
 */
public class DebitCardTest {

    public DebitCardTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDescription method, of class DebitCard.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        DebitCard instance = new DebitCard("DEBIT", "BANIF", "12283842", "Pedro Martins", DateTime.newCalendar(2016, 1, 1));
        String expResult = "DEBIT";
        String result = instance.cardName;
        assertEquals(expResult, result);
    }

}
