/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.domain;

import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Filipe
 */
public class IncomeTest {
    
    public IncomeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class Income.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testIncomePositive() {
        Calendar date = DateTime.newCalendar(2010, 10, 10);
        Income inc = new Income(-1, date, "Test Income");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testIncomePositive2() {
        Calendar date = DateTime.newCalendar(2010, 10, 10);
        Income inc = new Income(0, date, "Test Income");
    }
    
}
