/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author i070375
 */
public class IncomeTypeTest {
    
    public IncomeTypeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        // TODO review the generated test code and remove the default call to fail.
        IncomeType it = new IncomeType("teste");
        
        assertEquals("teste", it.getType());
        
    }
    
    @Test(expected=IllegalArgumentException.class) // java.lang.Exception
    public void exceptionTest() throws Exception {
        IncomeType itEmpty = new IncomeType("");
    }
   
    @Test(expected=IllegalArgumentException.class)
    public void exceptionTestNull() throws Exception {
        IncomeType itNull = new IncomeType(null);
    }
    
    
}
