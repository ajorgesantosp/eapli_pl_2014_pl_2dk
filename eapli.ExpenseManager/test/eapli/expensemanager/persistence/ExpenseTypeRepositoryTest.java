/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.expensemanager.persistence;

import eapli.expensemanager.persistence.inMemory.ExpenseTypeRepositoryimpl;
import eapli.expensemanager.domain.ExpenseType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class ExpenseTypeRepositoryTest {
    
    public ExpenseTypeRepositoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private ExpenseTypeRepositoryimpl instance;
    
    @Test
	public void ensureAddOne() {
		System.out.println("Add one");
		ExpenseType expenseType = new ExpenseType("not_null");
		instance.save(expenseType);

		int expected = 1;
		int actual = instance.count();
		assertEquals("One added should have count = 1", expected, actual);
	}

	@Test
	public void ensureEmptyHasSizeZero() {
		System.out.println("Empty repository must have count 0");

		int expected = 0;
		int actual = instance.count();
		assertEquals("Empty repository must have count 0", expected, actual);
	}

	@Test(expected = IllegalArgumentException.class)
	public void notAllowedToAddNull() {
		System.out.println("Not Allowed to add null");
		ExpenseType expenseType = null;
		instance.save(expenseType);
	}
    
    
    
    
}
